## Work hours logging/reporting system##
Suitable for small teams working in areas with poor cellular reception.
### How it works###
The foreman of the team sends an SMS message containing the list of employees, the ID number of the job and the number of hours worked. The System securely logs the data and sends confirmations to every team member (via SMS). The management via the dashboard on the website has access to aggregated data, can check/correct the input. The report is generated for every employee, with the number of hours to be paid.
Every employee has access to the employee dashboard, where they can see how many hours to be paid as well as their working history/statistics.
#### Why SMS and not an App?##
Because of poor wireless coverage in LA, SMS is much more reliable than wireless internet.
### Who can find its useful###
It was developed with moving companies in mind. Based on my experience working for them, moving companies are rather small organizations with poorly organized processes, but still, they employ dozens of people. Time tracking becomes a serious problem.
## Technology ##
Python/Django app relying on Twilio infrastructure for SMS communication


![admindash.PNG](https://bitbucket.org/repo/6LMGAL/images/163355228-admindash.PNG)
![employee summary.PNG](https://bitbucket.org/repo/6LMGAL/images/4013525472-employee%20summary.PNG)![job summary.PNG](https://bitbucket.org/repo/6LMGAL/images/3123672184-job%20summary.PNG)