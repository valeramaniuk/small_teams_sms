from django.contrib.auth.models import User
from twilio_app.models import Employee

def add_user(username, phone_number=None):
    #TODO: what will it do if, for example username has bad symbols?

    username = username.lower()
    phone_number =phone_number[1:]
    if User.objects.filter(username=username).exists():
        return None, None, "User {} exists, please choose a different name".format(username)

    if Employee.objects.filter(phone_number=phone_number).exists():
        return None, None, "User with the phone number {} exists. Text HELP for possible solutions".format(phone_number)

    user = User(username=username)
    password = User.objects.make_random_password(length=5,
                            allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789')
    user.set_password(password)
    user.save()
    return user, password, ""