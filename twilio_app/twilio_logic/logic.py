import re

from twilio_app.models import *
from twilio_app.twilio_logic import user_housekeeping
from twilio_app.twilio_logic.send_sms import send_confirmation



def select_action(msg, from_number):

    # helper function that determines if the message received
    # from an existing user, is fo -> proceed
    # if the sender is unknown ->
    if not Employee.objects.filter(phone_number=from_number).exists():
        return "Your phone number is not associated with any registered user." \
               "No changes were made."

    # If we cannot find an appropriate action
    # then just report "I dunno what you want"
    answer = "I don't understand. Please check your message or text HELP for instructions"


    # if contains "whoami" -> return username or tell him that
    # you dont know him
    message_calls_for_this_action, message = who_am_i(msg, from_number)
    if message_calls_for_this_action:
        answer = message

    # Check if user tries to add a NEW JOB
    # includes all confirmations
    message_calls_for_this_action, message = try_add_job(msg, from_number)
    if message_calls_for_this_action:
        answer = message

    # Check if user tries to add a NEW EMPLOYEE
    # includes all confirmations

    message_calls_for_this_action, message = try_add_employee(msg, from_number)
    if message_calls_for_this_action:
        answer = message
    return answer


def get_name_by_phone(from_number):
    # if phone number is not associated with user -> just return the phone number
    # if with multiple users -> return error message
    # but none of this should ever happen

    if Employee.objects.filter(phone_number=from_number).exists():
        if Employee.objects.filter(phone_number=from_number).count()==1:
            return Employee.objects.get(phone_number=from_number).user.username
        else:
            return "multiple users on the same phone"
    else:
        return from_number


def who_am_i(msg, from_number):
    if "whoami" in msg.lower():
        if Employee.objects.filter(phone_number=from_number).exists():
            return True, "{}".format(Employee.objects.get(phone_number=from_number).user.username)
        else:
            return True, "Your phone number is not registered with the System"
    else:
        return False, ""

def try_add_job(msg, from_number):

    # helper function that gets a list of names and returns
    # a list of names that don't match any registered user's username
    def is_unknown_user_list(user_list):
        is_unknown_users = False
        unknown_users_list =[]
        for name in user_list:
            if not User.objects.filter(username=name.lower()).exists():
                is_unknown_users = True
                unknown_users_list.append(name)
        if is_unknown_users:
            return unknown_users_list
        else:
            return None

    # msg ='1214# huita ivanovna, 5.5 hours, Valera, Denis'
    pattern = re.compile(r"(?P<job_number>[0-9a-zA-Z]+)#\s?,?\s?"
                         r"(?P<customer_name>[\w ]*)?,*\s*"
                         r"(?P<hours>[\d]+\.?[\d]*)?\s*(hours)?,*\s*"
                         r"(?P<employee_1>[\w]+),*\s*"
                         r"(?P<employee_2>[\w]+)?,*\s*"
                         r"(?P<employee_3>[\w]+)?,*\s*"
                         r"(?P<employee_4>[\w]+)?,*\s*"
                         r"(?P<employee_5>[\w]+)?,?\s*"
                         r"(?P<employee_6>[\w]+)?,?\s*"
                         r"(?P<employee_7>[\w]+)?,?\s*"
                         r"(?P<employee_8>[\w]+)?,?\s*")
    g = pattern.search(msg)
    # if pattern matches than user wants to add a job
    # but we still need to figure out the details
    if g:
        # gets the list of employees from the message
        user_list = []
        for i in range(1, 9):
            employee_index = "employee_" + str(i)
            if g.group(employee_index):
                user_list.append(g.group(employee_index))

        unknown_users_list = is_unknown_user_list(user_list)
        # gets a list of unknown employees
        # (from the list of employees in the message)
        # if the any unknown users - abort job adding and return
        # a list of unknown users back to sender for reference
        if unknown_users_list:
            unknown_users_str = ", ".join(unknown_users_list)
            return True, "{} are not registered users. If they are new users please register" \
                         " them (text HELP for instruction how), otherwise check spelling".format(unknown_users_str)



        job_number = g.group('job_number')

        # if job exist then just fetch it
        # if not then create


        if not Job.objects.filter(job_number=job_number).exists():
            job = Job(job_number=job_number)
            if g.group('customer_name'):
                job.customer_name = g.group('customer_name')
                job.added_by=get_name_by_phone(from_number)
            job.save()
        else:
            job = Job(job_number=job_number)

        # some jobs may don't have any hours info provided
        # so tentatively
        if g.group('hours'):
            hours = g.group('hours')
        else:
            hours = 0

        # create records for employees and send them
        # confirmation if they have a phone number in record
        for username in user_list:
            user = User.objects.get(username=username)
            record, created = Record.objects.get_or_create(job=job, user=user)
            record.hours = hours
            record.save()
            employee = Employee.objects.get(user=user)
            if employee.phone_number:
                if created:
                    msg = "{} hours for job: {} added to your account".format(hours, job_number)
                else:
                    msg = "UPDATED: now you have {} hours for job: {}".format(hours, job_number)
                send_confirmation(msg, employee.phone_number)

        #success, records saved
        if created:
            answer = 'Job# {} saved. {} hours for {}'.format(job_number, hours, ", ".join(user_list))
        else:
            # job was updated
            answer = 'Job# {} UPDATED. {} hours for {}'.format(job_number, hours, ", ".join(user_list))

        result = True
    else:
        # no pattern match ->
        # the message is not about job adding
        result = False
        answer = ''

    return result, answer


def try_add_employee(msg, from_number):

    pattern = re.compile(r"add\s*([a-z0-9_]+)\s(\d+)\s*$")
    g = pattern.search(msg)
    # if pattern even exists
    # we don't check for existence of individual groups
    # it may prove to be wrong and generate Exceptions

    if g:

        name = g.group(1)
        phone_number = g.group(2)
        user, password, error_msg = user_housekeeping.add_user(name, phone_number)
        if user is not None:
            emp = Employee(user=user, real_name=user.username)
            emp.phone_number = "+1"+ phone_number
            emp.save()
            confirmation_msg = " ".join(["You was added to online work hours tracking system by ", str(from_number),
                                         " http://hourstracker.online ",
                                         "username:", user.username, "password:", str(password), ])

            send_confirmation(confirmation_msg, emp.phone_number)
            return True, ' '.join(['Added', str(name), str(emp.phone_number)])
        else:
            return True, error_msg
    else:
        return False, ""
