import datetime

from twilio_app.models import Job, Record


def undo_delete_job(job_number):
    job = Job.deleted.get(job_number=job_number)
    job.is_deleted = False
    job.deleted_by = ""
    job.deleted_date = None
    job.save()


def delete_job(job_number, deleted_by):
    job = Job.active.get(job_number=job_number)
    job.is_deleted = True
    job.deleted_by = deleted_by
    job.deleted_date = datetime.datetime.now()
    job.save()


def delete_records_related_to_job(job_number, deleted_by):

    job =  Job.deleted.get(job_number=job_number)
    records = Record.active.filter(job=job)
    records.update( is_deleted=True,
                    deleted_date=datetime.datetime.now(),
                    deleted_by=deleted_by)


def undo_delete_records_related_to_job(job_number):

    job = Job.active.get(job_number=job_number)
    records = Record.deleted.filter(job=job)
    records.update( is_deleted=False,
                    deleted_date=None,
                    deleted_by="")