#!/usr/bin/env python2.7
from django.views.decorators.csrf import csrf_exempt
from twilio.rest import TwilioRestClient


@csrf_exempt
def send_confirmation(msg, phone_to):
    ACCOUNT_SID = "AC4dbbaaf2bfc2de2c294046f298470633"
    AUTH_TOKEN = "f2acb4e2174141f9419c0a31be2907a2"

    client = TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)
    if len(phone_to) == 12:
        client.messages.create(
            to=phone_to,
            from_='+13237989917',
            body= msg,
            # media_url='https://s-media-cache-ak0.pinimg.com/736x/92/bd/51/92bd51939ce6e27f773aee3516b2cd6f.jpg'
    )