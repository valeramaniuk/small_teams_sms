from django.contrib import admin
from .models import Record, Job, Employee, MessageLog, Faq, Salary

admin.site.register(MessageLog)
admin.site.register(Record)
admin.site.register(Job)
admin.site.register(Employee)
admin.site.register(Faq)
admin.site.register(Salary)

# Register your models here.
