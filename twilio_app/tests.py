from django.test import TestCase, RequestFactory
from twilio.twiml import Response
from .views import *
from django.test import Client


class EmployeeAddition(TestCase):
    """Adding a new employee by sending a tetx from existion one"""

    def setUp(self):
        # creating "existing" employee

        self.from_number = '+1555'
        self.admin_user_username = 'admin'

        user = User.objects.create(username=self.admin_user_username)
        user.set_password('pepsicola')

        user.save()
        Employee.objects.create(user=user, phone_number=self.from_number)
        self.factory = RequestFactory()
        self.name_phone_directory ={
            "new_dummy_1":'9999',
            "new_dummy_2":'6666',
            "new_dummy_3":'7777',
            "new_dummy_4":'8888',

        }


    def test_unknown_number(self):
        """Unknown numbers cant send any requests"""
        request = self.factory.post('/', {'Body': 'whoami', 'From': "438590"})
        response = react_to_sms(request)
        expected_message="Your phone number is not associated with any registered user.No changes were made."
        expected_response = Response()
        expected_response.message(expected_message)
        self.assertEqual(str(response), str(expected_response))

    def test_whoami(self):
        """whoami command returns associated name"""
        request = self.factory.post('/', {'Body': 'whoami', 'From': self.from_number})
        response = react_to_sms(request)
        expected_message =self.admin_user_username
        expected_response = Response()
        expected_response.message(expected_message)
        self.assertEqual(str(response), str(expected_response))


    def helper_add_new_employee(self,name, phone_number, from_number):
            message_to_send = 'Add '+str(name)+" "+str(phone_number)

            request = self.factory.post('/', {'Body': message_to_send,
                                          'From': from_number})
            return react_to_sms(request)

    def test_add_new_employee(self):
        """existing Employee can add a new one"""



        for name in self.name_phone_directory:
            phone = self.name_phone_directory[name]

            response = self.helper_add_new_employee(name, phone, self.from_number)

            # acknowledge addition

            expected_message= "Added "+name+" "+str(phone)
            expected_response = Response()
            expected_response.message(expected_message)
            self.assertEqual(str(response), str(expected_response))

            # check if new emlployee really exists
            self.assertTrue(User.objects.get(username=name))
            self.assertTrue(Employee.objects.get(user__username=name, phone_number=phone))

        # now each one of the new employees will try to add a "son"
        # his names will be _son and phone extension 000
        for name in self.name_phone_directory:
            phone = self.name_phone_directory[name]

            response = self.helper_add_new_employee(name+"_son", phone+'000', phone)

            # acknowledge addition

            expected_message= "Added "+name+"_son "+phone+'000'
            expected_response = Response()
            expected_response.message(expected_message)
            self.assertEqual(str(response), str(expected_response))

            # check if new emlployees really exists
            self.assertTrue(User.objects.get(username=name+'_son'))
            self.assertTrue(Employee.objects.get(user__username=name+"_son", phone_number=phone+"000"))

        # after son spawning there should be 4x2 + original admin = 9 employees
        self.assertEqual(Employee.objects.count(), 9)

    def test_adding_new_job(self):
        """Adding a new job by SMS"""
        team1 =["dummy1", "dummy2"]
        team2 =["Dummy2", "Dummy3"]
        team3 = ["dummy3", "dummy4"]
        team4 = ["dummy5", "dummy4"]

        teams = [team1, team2, team3, team4]

        job_list_1 =[
            ["0001", "Buryachok1", "4.5", team1],
            ["0002", "Buryachok2", "4.0", team2],
            ["0003", "Buryachok3", "4.25", team1],
            ["0004", "Buryachok4", "4.75", team2],
                   ]
        job_list_2 =[
            ["0001", "Buryachok1", "4.5", team3],
            ["0002", "Buryachok2", "5.0", team4],
            ["0003", "Buryachok3", "4.25", team2],
            ["0004", "Buryachok4", "5.50", team1],
                   ]

        self.helper_add_new_employee('dummy1', '7868', '+1555' )
        self.helper_add_new_employee('dummy2', '7869', '+1555' )
        self.helper_add_new_employee('dummy3', '7870', '+1555' )
        self.helper_add_new_employee('dummy4', '7871', '+1555' )
        self.helper_add_new_employee('dummy5', '7872', '+1555' )


        def add_job_string_composer_v1(job_number, customer_name, hours, team):
            # 555# Customer 7.25 Ben, Tom
            message_to_send = job_number+"# "+customer_name+" "+hours+" "+" ".join(team)
            return message_to_send

        def add_job_string_composer_v2(job_number, customer_name, hours, team):
            # 555# Customer 7.25 hours Ben, Tom
            message_to_send = job_number+"# "+customer_name+" "+hours+" hours"+" ".join(team)
            return message_to_send

        def add_job_string_composer_v3(job_number, customer_name, hours, team):
            # 555# Customer, 7.25, Ben, Tom
            message_to_send = job_number+"# "+customer_name+", "+hours+", "+", ".join(team)
            return message_to_send


        def add_bunch_of_jobs(job_list):

            list_of_formats = [
                add_job_string_composer_v1,
                add_job_string_composer_v2,
                add_job_string_composer_v3,
            ]


            for job, format_function in zip(job_list, list_of_formats):

                job_number=job[0]
                customer_name = job[1]
                hours= job[2]
                team=job[3]

                message_to_send = format_function(*job)
                request = self.factory.post('/', {'Body': message_to_send,
                                              'From': self.from_number})
                response = react_to_sms(request)

                expected_message = "Job# "+job_number+" saved. "+hours+" hours for "+ ", ".join(team).lower()
                expected_response = Response()
                expected_response.message(expected_message)
                self.assertEqual(str(response), str(expected_response))

        # adding bunch of jobs
        add_bunch_of_jobs(job_list_1)
        # updating/ adding jobs
        add_bunch_of_jobs(job_list_2)

    # def test_add_job_with_unknown_emploees(self):
    #
    #     teamX = ["UnknownGyu", "whoisthisguy"]
    #     message_to_send=add_job_string_composer_v1('000000', "Vinnie the Pooh", '9', teamX)
    #     request = self.factory.post('/', {'Body': message_to_send,
    #                                       'From': self.from_number})
    #     response = react_to_sms(request)
    #     print(response)




class JobDeletionCase(TestCase):
    def setUp(self):
        user = User.objects.create(username="dummy1")
        Employee.objects.create(user=user, real_name="dummy One")

        user = User.objects.create(username="dummy2")
        Employee.objects.create(user=user, real_name="dummy Two")

        user = User.objects.create(username="dummy3")
        Employee.objects.create(user=user, real_name="dummy Three")

        job = Job.objects.create(job_number='9999', customer_name="Dummy Customer")

        user = User.objects.get(username='dummy1')
        Record.objects.create(job=job, user=user)

        user = User.objects.get(username='dummy2')
        Record.objects.create(job=job, user=user)

        user = User.objects.get(username='dummy3')
        Record.objects.create(job=job, user=user)

    def test_job_exists_and_is_active(self):
        Job.active.get(job_number='9999')

    def test_records_exist_and_active(self):
        number_of_records = Record.active.filter(job__job_number='9999').count()
        number_of_deleted_records = Record.deleted.filter(job__job_number='9999').count()
        self.assertEqual(number_of_records, 3)
        self.assertEqual(number_of_deleted_records, 0)

    def test_delete_job(self):
        """Delete Job"""

        delete_job('9999', 'deleted_by')
        job = Job.deleted.get(job_number='9999')
        self.assertEqual(job.is_deleted, True)
        self.assertEqual(job.deleted_by, "deleted_by")
        self.assertNotEqual(job.deleted_date, None)

    def test_delete_records_related_to_job(self):
        """Records become deleted/undeleted by the number of the job"""

        '''Job deleted then
            Corresponding records deleted, then
            Job UNdeleted
            Corresponding records undeleted'''

        job_number = '9999'
        delete_job('9999', 'deleted_by')
        job = Job.deleted.get(job_number=job_number)

        delete_records_related_to_job(job_number, "dude")

        deleted_records = Record.deleted.filter(job=job)
        active_records = Record.active.filter(job=job)
        deleted_by_list = Record.deleted.values_list('deleted_by', flat=True)
        deleted_dates_list = Record.deleted.values_list('deleted_date', flat=True)

        self.assertNotEqual(list(deleted_dates_list), [None, None, None])
        self.assertEqual(deleted_records.count(), 3)
        self.assertEqual(active_records.count(), 0)
        self.assertEqual(list(deleted_by_list), ['dude', 'dude', 'dude'])

        undo_delete_job('9999')

        job = Job.active.get(job_number='9999')
        self.assertEqual(job.is_deleted, False)
        self.assertEqual(job.deleted_by, "")
        self.assertEqual(job.deleted_date, None)

        undo_delete_records_related_to_job(job_number)
        deleted_records = Record.deleted.filter(job=job)
        active_records = Record.active.filter(job=job)
        deleted_by_list = Record.deleted.values_list('deleted_by', flat=True)

        self.assertEqual(deleted_records.count(), 0)
        self.assertEqual(active_records.count(), 3)
        self.assertEqual(list(deleted_by_list), [])
