from django import template

register = template.Library()

from ..models import Employee


@register.filter('has_group')
def has_group(user, group_name):

    groups = user.groups.all().values_list('name', flat=True)
    return True if group_name in groups else False


@register.simple_tag
def job_status_table_row(status):
    if status == 'paid':
        return ''
    if status == 'confirmed':
            return 'success'
    if status == 'draft':
            return 'warning'
    if status == 'mixed':
            return 'danger'

@register.simple_tag
def record_status_table_row(status):
    if status == 'paid':
        return ''
    if status == 'confirmed':
            return 'success'
    if status == 'draft':
            return 'warning'


@register.simple_tag
def status_button(action, job_status):
    if action == job_status:
        return 'btn btn-primary'
    else:
        return 'btn btn-default'

@register.inclusion_tag("twilio_app/tags/salary_for_position.html")
def salary_for_position(record):
    salary=record.get_salary(record.position)
    return {"salary":salary}


@register.inclusion_tag("twilio_app/tags/full_position_title.html")
def full_position_title(position):
    if position == 'H':
        return {"position": "Helper"}
    if position == 'D':
        return {"position": "Driver"}
    if position == 'F':
        return {"position": "Foreman"}
    if position == 'FD':
        return {"position": "Foreman/Driver"}


@register.inclusion_tag('twilio_app/tags/glyphicons_status.html')
def glyphicon_for_record(record):
    status = record.status
    return {"status": status}

@register.inclusion_tag('twilio_app/tags/glyphicons_status.html')
def status_to_glyph(status):
    return {"status": status}


@register.inclusion_tag('twilio_app/tags/hours_and_extra.html')
def hours_and_extra(record):

    return {"record": record}


@register.inclusion_tag('twilio_app/tags/tips_and_bonuses.html')
def tips_and_bonuses(record):

    return {"record": record}

