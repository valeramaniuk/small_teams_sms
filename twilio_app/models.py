import datetime
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models import Sum
from django.core.exceptions import ObjectDoesNotExist
from django.utils.dateformat import DateFormat

POSITION_CHOICES = {('D', 'Driver'), ('H', 'Helper'), ('F', 'Foreman'), ("FD", "Foreman and Driver")}
STATUS_CHOICE = {('draft', 'Draft'), ('paid', 'Paid'), ('confirmed', 'Confirmed'), ('mixed', 'Mixed')}


class NotDeletedManager(models.Manager):
    def get_queryset(self):
        return super(NotDeletedManager, self).get_queryset().filter(is_deleted=False)


class DeletedManager(models.Manager):
    def get_queryset(self):
        return super(DeletedManager, self).get_queryset().filter(is_deleted=True)


class Position(models.Model):

    abbr = models.CharField(max_length=5)
    title = models.CharField(max_length=20)


class Employee(models.Model):

    user = models.OneToOneField(User, primary_key=True, related_name="employee")
    real_name = models.CharField(max_length=100, default='')
    possible_position = models.CharField(max_length=50, choices=POSITION_CHOICES, default='H')
    phone_number = models.CharField(max_length=12, blank=True)
    comments = models.TextField(blank=True)
    is_deleted=models.BooleanField(default=False)
    deleted_date = models.DateTimeField(null=True)
    deleted_by = models.CharField(max_length=100, blank=True)


    def __str__(self):
        return str(self.real_name)


class Job(models.Model):
    objects = models.Manager()
    active = NotDeletedManager()
    deleted = DeletedManager()

    job_number = models.CharField(max_length=20, primary_key=True)
    job_date = models.DateTimeField(default=timezone.now)
    date_last_modified = models.DateField(auto_now=True)
    customer_name = models.CharField(max_length=100, blank=True)
    comments = models.CharField(max_length=200, blank=True)
    status = models.CharField(max_length=100, default='draft', choices=STATUS_CHOICE)
    added_by = models.CharField(max_length=20, blank=True)
    is_deleted=models.BooleanField(default=False)
    deleted_date = models.DateTimeField(null=True)
    deleted_by = models.CharField(max_length=100, blank=True)

    #  it may be obsolete
    # def update_delete_date(self):
    #     self.deleted_date = datetime.datetime.now()
    #     self.save()

    def refresh_status(self):
        records= Record.objects.filter(job=self)
        list_of_statuses = records.values_list('status', flat=True)
        if len(set(list_of_statuses)) > 1:
            self.status = 'mixed'
        else:
            self.status = list_of_statuses[0]
        self.save()

    def get_team(self):

        records = Record.objects.filter(job=self)
        list_of_users = records.values_list('user__username', flat=True)
        return list_of_users

    def get_team_compliment(self):
        # returns usernames that are not Managers
        # and are not already listed in this Job
        usernames = list( User.objects.all().\
                        exclude(groups__name__contains='managers').\
                        values_list('username', flat=True))
        team = list(self.get_team())
        for user in team:
            try:
                usernames.remove(user)
            except ValueError:
                pass
        return usernames


    class Meta:
        ordering =['-job_date']

    def __str__(self):

        df = DateFormat(self.job_date)
        date = df.format('m-d')
        return "{} ({}) {}".format(self.job_number, self.customer_name, date)


class Record(models.Model):
    objects =  models.Manager()
    active =NotDeletedManager()
    deleted =  DeletedManager()

    user = models.ForeignKey(User, related_name='record', default=None)
    job = models.ForeignKey(Job, related_name='job', blank=True)
    position = models.CharField(max_length=20, choices=POSITION_CHOICES, default="H")
    hours = models.FloatField(default=0)
    extra_hours = models.FloatField(default=0)
    bonus = models.FloatField(default=0)
    tips = models.FloatField(default=0)
    status = models.CharField(max_length=100, default="draft", choices=STATUS_CHOICE)
    is_deleted=models.BooleanField(default=False)
    deleted_date = models.DateTimeField(null=True)
    deleted_by = models.CharField(max_length=100, blank=True)

    def get_salary(self, position):
        """returns the value of the salary at the moment of the
        latest salary change"""
        date = self.job.job_date
        try:
            salaries_that_older=Salary.objects.filter(user=self.user,
                                              date__lt=date,
                                                      position=position,
                                          ).latest('date')
            return salaries_that_older.per_hour
        except ObjectDoesNotExist:
            return None

    def get_payment_for_this_job(self):
        """based on position, salary at the moment and hours
        returns dollar amount"""
        salary = self.get_salary(self.position)
        if salary:
            pay = salary * (self.hours+self.extra_hours)
            return pay
        else:
            return 0

    class Meta:
        unique_together = ('user', 'job')




    def __str__(self):
        df = DateFormat(self.job.date_last_modified)
        date = df.format('m/d')
        return "{} job#:{}, {}".format(self.user.username,  self.job.job_number, date)


class Salary(models.Model):
    user = models.ForeignKey(User)
    position = models.CharField(max_length=20, choices=POSITION_CHOICES)
    per_hour = models.FloatField()
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.user.username +" " +self.position +" $"+str(self.per_hour)+" "+str(self.date)

class MessageLog(models.Model):
    body = models.CharField(max_length=250)
    from_number = models.CharField(max_length=250, blank=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.body


class Faq(models.Model):
    question = models.CharField(max_length=500)
    answer = models.TextField()

    def __str__(self):
        return self.question
