from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^job_details/(?P<job_number>[0-9a-z]+)?/$', views.job_details, name='job_details'),
    url(r'^edit_job_details/(?P<job_number>[0-9a-z]+)?$', views.edit_job_details, name='edit_job_details'),
    url(r'^employee_details/(?P<username>[a-z0-9]*)', views.employee_details,
        name='employee_details'),

    url(r'^dashboard/(?P<record_status_filter>[a-z_]+)?$', views.dashboard, name='dashboard'),
    url(r'^admin-dashboard/(?P<tab_status>[a-z_]+)?/?(?P<job_status_filter>[a-z_]+)?$', views.admin_dashboard,
        name='admin_dashboard'),
    url(r'^reply/', views.react_to_sms, name='react_to_sms'),
    url(r'^test/', views.test_sms),
    url(r'^generate_database/$', views.generate_database, name='generate_database'),
    url(r'^edit_job_status/$', views.edit_job_status, name='edit_job_status'),
    url(r'^edit_record_status/$', views.edit_record_status, name='edit_record_status'),
    url(r'^add_employee_to_job/$', views.add_employee_to_job, name='add_employee_to_job'),
    url(r'^del_employee_from_job/$', views.del_employee_from_job, name='del_employee_from_job'),
    url(r'^del_job/$', views.del_job, name='del_job'),

]
