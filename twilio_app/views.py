from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import PageNotAnInteger, \
    Paginator, EmptyPage
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django_twilio.decorators import twilio_view
from twilio.twiml import Response

from twilio_app.helpers import job_status_propagate
from twilio_app.twilio_logic import logic

from twilio_app.twilio_logic.job_and_record_deletion_and_restoring import undo_delete_job, delete_job, \
    delete_records_related_to_job, undo_delete_records_related_to_job

from .forms import LoginForm
from .helpers import fake_data_loader
from .models import *


def group_required(*group_names):
    """Requires user membership in at least one of the groups passed in."""
    def in_groups(u):
        if u.is_authenticated():
            if bool(u.groups.filter(name__in=group_names)) | u.is_superuser:
                return True
        return False
    return user_passes_test(in_groups)


@login_required
@group_required('managers')
def employee_details(request, username):
    user = User.objects.get(username=username)
    employee = Employee.objects.get(user=user)
    records_list =  Record.active.filter(user=user)
    paginator = Paginator(records_list, 8)
    record_page = request.GET.get('page')
    try:
        records = paginator.page(record_page)
    except PageNotAnInteger:
        records = paginator.page(1)
    except EmptyPage:
        records = paginator.page(paginator.num_pages)




    context = {'user': user,
               'employee': employee,
               'records': records,
               'page':record_page}
    return render(request, 'twilio_app/employee_details.html', context)


@login_required
@group_required('managers')
def admin_dashboard(request, tab_status, job_status_filter):
    #TODO : convert tab status and filter to forms
    """
    only management personel
    access restricted by permissions
    :param tab_status:
    """
    if tab_status is None:
        tab_status = 'jobs'

    records = Record.active.all().order_by('job__job_date')
    employees = Employee.objects.all()
    sms_messages = MessageLog.objects.all()
    number_jobs_to_confirm = Job.active.filter(status='draft').count()

    if job_status_filter:
        jobs_list = Job.active.all().filter(status=job_status_filter)

    else:
        jobs_list = Job.active.all()

    deleted_jobs = Job.deleted.all()

    paginator = Paginator(jobs_list, 8)
    job_page = request.GET.get('page')
    try:
        jobs = paginator.page(job_page)
    except PageNotAnInteger:
        jobs = paginator.page(1)
    except EmptyPage:
        jobs = paginator.page(paginator.num_pages)

    users = User.objects.all().exclude(groups__name__icontains='managers')
    context = {'records': records,
               'jobs': jobs,
               'deleted_jobs': deleted_jobs,
               'number_jobs_to_confirm':number_jobs_to_confirm,
               'page': job_page,
               'employees': employees,
               'tab_status': tab_status,
               'users': users,
               'sms_messages': sms_messages,
               }
    return render(request, 'twilio_app/admin_dashboard.html', context)


@login_required
def dashboard(request, record_status_filter=None):

    user = request.user
    employee = Employee.objects.get(user=user)

    if record_status_filter is None:
        records_list = Record.active.filter(user=user).order_by('job')
    else:
        records_list = Record.active.filter(user=user, status=record_status_filter)

    paginator = Paginator(records_list, 8)
    page = request.GET.get('page')
    try:
        records = paginator.page(page)
    except PageNotAnInteger:
        records = paginator.page(1)
    except EmptyPage:
        records = paginator.page(paginator.num_pages)

    hours_need_to_be_confirmed = Record.active.filter(status='draft', user=user).aggregate(Sum('hours'))[
        'hours__sum']
    jobs_need_to_be_confirmed = Record.active.filter(status='draft', user=user).count()

    jobs_to_pay = Record.active.filter(status='confirmed', user=user).count()
    hours_to_pay = Record.active.filter(status='confirmed', user=user).aggregate(Sum('hours'))[
        'hours__sum']

    context = {'records': records,
               'employee': employee,
               'page': page,
               'hours_need_to_be_confirmed': hours_need_to_be_confirmed,
               'jobs_need_to_be_confirmed': jobs_need_to_be_confirmed,
               'hours_to_pay': hours_to_pay,
               'jobs_to_pay': jobs_to_pay
               }

    return render(request, 'twilio_app/personal_dashboard.html', context)


def user_logout(request):
    logout(request)
    return redirect('index')


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'],
                                password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    messages.add_message(request, messages.SUCCESS, 'Authorization successful.')
                    if user.groups.filter(name = "managers").exists():
                        return redirect('admin_dashboard')
                    else:
                        return redirect('dashboard')
                else:
                    messages.add_message(request, messages.INFO, 'This user is suspended')
                    return render(request, 'login.html', {'form': form})
            else:
                messages.add_message(request, messages.WARNING, 'Authorization FAILED.')
                return render(request, 'login.html', {'form': form})
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


def index(request):

    return render(request, 'twilio_app/index.html')


@login_required()
@group_required('managers')
def edit_job_details(request, job_number=None):
    if request.method =='POST':
        job_number = request.POST['job_number']

        '''if CANCEL button'''
        if 'cancel' in request.POST:
            messages.add_message(request, messages.WARNING, 'Cancelled. No changes saved.')
            return redirect('job_details', job_number)

        job = Job.active.get(job_number=request.POST['job_number'])
        all_record_id = Record.objects.filter(job=job).values_list('id')
        job.customer_name = request.POST['customer_name']
        job.comments = request.POST['comments']

        job.save()

        for record_id in all_record_id:
            record = Record.active.get(id=int(record_id[0]))
            position = request.POST['position_for_record_' + str(record_id[0])]
            hours = request.POST['hours_for_record_' + str(record_id[0])]
            tips = request.POST['tips_for_record_' + str(record_id[0])]
            extra_hours = request.POST['extra_hours_for_record_' + str(record_id[0])]
            bonus = request.POST['bonus_for_record_' + str(record_id[0])]
            tips = tips.replace(',','.')
            try:
                float(tips)
                float(bonus)
            except ValueError:
                messages.add_message(request, messages.WARNING, 'Tips and Bonus should be numbers.'
                                                                'NOTHING was saved')
                return redirect('job_details', job_number)

            record.position = position
            record.hours = hours
            record.tips = tips
            record.extra_hours = extra_hours
            record.bonus = bonus


            record.save()
            # print(request.POST['position_for_record_'+str(record_id[0])])


        messages.add_message(request, messages.SUCCESS, 'Changes saved')

        return redirect('job_details', job_number)
    else:
       job = Job.objects.get(job_number=job_number)
       records = Record.objects.filter(job=job)
       positions_list = Position.objects.all()

       i=0.0
       hours_list=[]
       while (i< 15):
           hours_list.append(i)
           i+=0.25




       context ={"job":job,
                 "records":records,
                 "positions_list":positions_list,
                 "hours_list":hours_list}
    return  render(request, 'twilio_app/managers/edit_job_details.html', context)





@login_required()
@group_required('managers')
def edit_job_status(request):
    job = Job.active.get(job_number=request.POST['job_number'])
    if "button_job_confirmed" in request.POST:
        job.status='confirmed'
    if "button_job_paid" in request.POST:
        job.status='paid'

    if "button_job_draft" in request.POST:
        job.status='draft'

    job.save()
    job_status_propagate.job_to_records_propagate(job)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required()
@group_required('managers')
def edit_record_status(request):
    record= Record.active.get(id=request.POST['record_id'])
    if "button_record_confirmed" in request.POST:
        record.status='confirmed'
    if "button_record_paid" in request.POST:
        record.status='paid'
    if "button_record_draft" in request.POST:
        record.status='draft'
    record.save()
    record.job.refresh_status()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))



@login_required
@group_required('managers')
def job_details(request, job_number):

    records = Record.active.filter(job__job_number=job_number)
    job = Job.active.get(job_number=job_number)
    usernames = job.get_team_compliment()
    positions = Position.objects.all()
    context = {'records': records,
               'job': job,
               'usernames':usernames,
               'positions':positions}

    return render(request, 'twilio_app/records_for_job.html', context)

@login_required
@group_required('managers')
def add_employee_to_job(request):
    job = Job.active.get(job_number=request.POST['job_number'])
    user = User.objects.get(username=request.POST['username'])
    new_record = Record(job=job, user=user)
    new_record.save()

    messages.add_message(request, messages.SUCCESS, 'Employee '+user.username+" added")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

@login_required
@group_required('managers')
def del_employee_from_job(request):
    record = Record.active.get(id=request.POST['record_id'])
    record.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
@group_required('managers')
def del_job(request):

    job_number = request.POST["job_number"]

    if "button_undo_del_job" in request.POST:
        undo_delete_job(job_number)
        undo_delete_records_related_to_job(job_number)

    if "button_del_job" in request.POST:
        delete_job(job_number, request.user.username)
        delete_records_related_to_job(job_number, request.user.username)

    # records = Record.objects.all()
    return  HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@twilio_view
# @csrf_exempt
def react_to_sms(request):
    """
    gets a message and a number of the sender
    and passes to another function which, in turn
    will do some work and then return a report message.
    This message will be forwarded to Twilio and become an SMS
    to the sender.
    Also, it logs every SMS along with the phone number
    :param request:
    """
    body = request.POST.get('Body', '')
    from_number = request.POST.get('From', '')
    l = MessageLog(body = str(body), from_number=str(from_number))
    l.save()
    body=body.lower()

    msg = logic.select_action(body, from_number)
    r = Response()

    r.message(msg)
    return r

@csrf_exempt
def test_sms(request):
    logic.send_confirmation()



def generate_database(request):
    fake_data_loader.main()
    return redirect('index')

