from django import forms
from django.contrib.auth.models import User

from .models import Employee

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class DropDown(forms.Form):
    users =  User.objects.all().values_list('id','username')
    ch = forms.ChoiceField(choices=users)
