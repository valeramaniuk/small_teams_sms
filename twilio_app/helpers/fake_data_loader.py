from django.contrib.auth.models import Group
import datetime
from twilio_app.models import *
import random, logging



job_list=[
    "55525,Fuckface,comments,draft",
    "65466,DickHead,yelp recommended,draft",
    "72377,SpiderMan,weaves the web,paid",
    "83488,Sponge Bob,yellow pants,confirmed",
    "99569,Superman,S,confirmed",
    "13231,Peter Griffin,fat,paid",
    "00312,Stewie,gay,draft",
    "94384,Meg,has a cap and ugly,draft",
    "85844,Brian,is a dog,draft",
    "26753,Quagmire,Gigiddy!,draft",
    "90383,Mayor Adams,nuts,paid",
    "73834,Homes Simpson,like doughtnuts,draft",
    "82922,Bart Simpson,like to sketeboard and stuff,draft",
    "92323,Beavis,he'll score oh yeah!,draft",
    "25372,ButtHead,cornhollio!!,draft",
    "93434,Gerald,likes potions nad decoctions,draft",
    "93213,Murazor,bold and has a beard,draft",
    "43522,Jove, has a youtube channel,draft",
    "43257,Ricky Bobby, believes in baby Jesus,draft",
    "44257,Bobby, believes in baby Jesus,draft",
    "43258,Ricky Bobby, believes in baby Jesus,draft",
    "43259,Ricky Bobby, believes in baby Jesus,draft",
    "43250,Ricky Bobby, believes in baby Jesus,draft",
    "43240,Ricky Bobby, believes in baby Jesus,draft",
    "43241,Ricky Bobby, believes in baby Jesus,draft",
    "43242,Ricky Bobby, believes in baby Jesus,draft",
    "43243,Ricky Bobby, believes in baby Jesus,draft",
    "43244,Ricky Bobby, believes in baby Jesus,draft",
    "43240,Ricky Bobby, believes in baby Jesus,draft",

]

user_list=["valera","kolya","geka","sergei","sashapasadena","filippio",]
positions = [["H","Helper"],["D","Driver"],["F","Foreman"],["FD","Foreman/Driver"],]

def get_phone_number():
    phone_number=''
    for _ in range(10):
        phone_number += str(random.randrange(0,9))
    return phone_number

def database_delete():

    Job.objects.all().delete()
    Employee.objects.all().delete()
    User.objects.all().delete()
    Record.objects.all().delete()
    Group.objects.all().delete()
    Position.objects.all().delete()

def main():
    database_delete()
    group = Group(name="managers")
    group.save()

    admin = User(username='admin', is_superuser=True, is_staff=True)
    admin.set_password('negative1988')
    admin.save()
    admin.groups.add(group)
    admin.save()


    for k,v in positions:
        p=Position(abbr=k, title=v)
        p.save()

    for username in user_list:
        user,is_created=User.objects.get_or_create(username=username)
        user.set_password("pepsicola")
        user.save()
        emp = Employee(user=user,real_name=username, phone_number=get_phone_number())
        emp.save()





    for days_back, entry in enumerate(job_list):
        a = datetime.datetime.today()
        new_date= a - datetime.timedelta(days=days_back)


        fields = entry.split(',')
        j=Job()
        j.job_number = int(fields[0])
        j.customer_name = fields[1]
        j.comments = fields[2]
        j.status = fields[3]
        j.job_date=new_date
        j.save()


    jobs_objects = Job.objects.all()
    for job in jobs_objects:
        user_objects = User.objects.all().exclude(groups__name__contains='managers')

        team =set()
        team_size = random.randint(2,5)
        while len(team)<team_size:
            team.add(random.choice(user_objects))

        for user in team:
            hours=random.randint(3,12)
            hours_decimal = random.randint(0,3)/4.0
            hours+=hours_decimal

            extra_hours =[0,0,0,0,0,1,1.5,2]
            bonus =[0,0,0,0,0,0,0,0,25,25,40]
            tips=[0,0,0,0,0,0,25,25,50]
            status_list=["draft", "paid","paid","confirmed"]
            position_list = ["H","D","F","FD"]
            record,is_created = Record.objects.get_or_create(
                            job=job,
                            user=user,
                            hours = hours,
                            extra_hours=random.choice(extra_hours),
                            bonus=random.choice(bonus),
                            status=random.choice(status_list),
                            tips=random.choice(tips),
                            position=random.choice(position_list))
            record.save()


