from twilio_app.models import Record


def job_to_records_propagate(job):
    new_status = job.status
    records = Record.objects.filter(job=job)
    for record in records:
        record.status =  new_status
        record.save()